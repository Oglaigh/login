<%-- 
    Document   : error
    Created on : 14-sep-2018, 20:50:07
    Author     : andra
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Error</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MC<w98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <div class="container" style="margin-top:20px;">
            <div class="col-md-6" style="border:1px solid #ccc;padding:20px 15px;margin:0 auto;">
            <h1>Error</h1>
            <div class="alert alert-danger">${mensajeError}</div>
            <a href="./" class="btn btn-secondary">Volver</a>
            </div>
        </div>
    </body>
</html>
