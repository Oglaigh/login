<%-- 
    Document   : admin
    Created on : 24-sep-2018, 22:36:40
    Author     : andra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Menú de administrador</title>
    </head>
    <body>
        <h2>Bienvenido Admin! ${usuarioElegido}</h2>
    </body>
</html>