<%-- 
    Document   : index
    Created on : 14-sep-2018, 19:59:42
    Author     : andra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Login</title>
    </head>
    <body>
        <div class="container" style="margin-top:20px;">
            <div class="col-md-4 col-md-offset-4" style="border:1px solid #ccc;padding:20px 15px;margin:0 auto;">
                <h1 class = "centrar"> Login </h1>
                <form method="post" action="Controlador">
                    <p class="centrar">
                        <label for="usuario">Usuario:</label>
                        <input type = "text" id="usuario" class="form-control" name = "usuario">
                    </p>
                    <p class="centrar">
                        <label for="password">Contraseña:</label>
                        <input type = "password" id="password" class="form-control" name = "password">
                    </p>
                    <p class="centrar">
                        <button type="submit" name="entrar" class="btn btn-primary">Iniciar sesión</button>
                    </p>
                        <input type="hidden" name="dirIP" value="localhost">
                        <input type="hidden" name="nomBD" value="ControlAcceso">

                </form>
            </div>
        </div>
    </body>
</html>

