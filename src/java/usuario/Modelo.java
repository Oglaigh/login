/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
/**
 *
 * @author andra
 */
public class Modelo {

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private UserBean resultado;
    private ActionListener listener;

    public Modelo(String url, String dbName) {
        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;
        resultado = new UserBean();
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    public void consultarUsuario(String u,String p) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "SELECT nombre, password, rol "
                            + "FROM usuarios "
                            + "WHERE nombre='" + u +"' AND password= '"+p+"';");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                UserBean usuario = new UserBean();
                usuario.setUser(rs.getString(1));
                usuario.setPass(rs.getString(2));
                usuario.setRol(rs.getString(3));
                resultado = usuario;
            }
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }

    }

    public UserBean getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }
}

