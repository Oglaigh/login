/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 *
 * @author andra
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class Controlador extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws IOException, ServletException {
        this.request = request;
        this.response = response;
        String ip = request.getParameter("dirIP");
        String bd = request.getParameter("nomBD");
        String usuario = request.getParameter("usuario");
        String password = request.getParameter("password");
        Modelo m = new Modelo(ip, bd);
        m.addExceptionListener(new ExceptionListener());
        m.consultarUsuario(usuario,password);
       
        if (usuario.isEmpty() || password.isEmpty()) { // VALIDA QUE NO ESTEN VACIOS LOS CAMPOS
            request.setAttribute("mensajeError", "Debe ingresar un usuario y contraseña");
            RequestDispatcher vista = request.getRequestDispatcher("error.jsp");
            vista.forward(request, response);
        }else if(m.getResultado().getUser() == null){ // VALIDAR SI EL USUARIO EXISTE
            request.setAttribute("mensajeError", "El usuario y/o la contraseña no son correctas.");
            RequestDispatcher vista = request.getRequestDispatcher("error.jsp");
            vista.forward(request, response);
        }else{ // SI LOS CAMPOS NO ESTAN VACIOS Y EL USUARIO INGRESADO EXISTE
        if(m.getResultado().getRol().equals("Administrador")){
            request.setAttribute("usuarios", m.getResultado());
            request.setAttribute("usuarioElegido", usuario);
        
            RequestDispatcher vista = request.getRequestDispatcher("admin.jsp");
            vista.forward(request, response);
        }
        else if(m.getResultado().getRol().equals("Moderador")){
            request.setAttribute("usuarios", m.getResultado());
            request.setAttribute("usuarioElegido", usuario);
        
            RequestDispatcher vista = request.getRequestDispatcher("moderador.jsp");
            vista.forward(request, response);
        }else{
            request.setAttribute("usuarioElegido", usuario);
        
            RequestDispatcher vista = request.getRequestDispatcher("invitado.jsp");
            vista.forward(request, response);
        }}
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String exception = event.getActionCommand();
            request.setAttribute("mensajeError", exception);
            RequestDispatcher vista = request.getRequestDispatcher("error.jsp");
            try {
                vista.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
